import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * The Gui class extends the Java Swing JFrame class.
 * It provides a graphical interface to the Calculator class.
 */
public class Gui extends JFrame
{
  JTextField inputTextField;
  JTextField outputTextField;
  JButton zeroButton, oneButton, twoButton, threeButton, fourButton, fiveButton, sixButton, sevenButton, eightButton, nineButton;
  JButton openBracketButton, closeBracketButton, decimalPointButton, answerButton, clearButton, clearAllButton;
  JButton additionButton, subtractionButton, multiplicationButton, divisionButton;
  int inputTextFieldSize = 20;
  int outputTextFieldSize = 20;
  InputHandler handler;
  CalculatorObject calc = new CalculatorObject();
  
  /*
   * Creates the gui interface for the calculator and initializes
   * all the objects it uses.
   */
  public Gui()
  {
    super("Calculator");
    Container container = getContentPane();
    container.setLayout(new GridLayout(6,4));
    handler = new InputHandler();
    //Text field setup.
    inputTextField = new JTextField(inputTextFieldSize);
    outputTextField = new JTextField("", outputTextFieldSize);
    outputTextField.setEditable(false);
    container.add(inputTextField);
    container.add(outputTextField);
    inputTextField.addActionListener(handler);
    //Empty text fields to pad the top row of the grid.
    JTextField dummyTextField = new JTextField(outputTextFieldSize);
    dummyTextField.setEditable(false);
    container.add(dummyTextField);
    JTextField dummyTwoTextField = new JTextField(outputTextFieldSize);
    dummyTwoTextField.setEditable(false);
    container.add(dummyTwoTextField);
    //Button setup.
    oneButton = new JButton("1");
    oneButton.addActionListener(handler);
    container.add(oneButton);
    twoButton = new JButton("2");
    twoButton.addActionListener(handler);
    container.add(twoButton);
    threeButton = new JButton("3");
    threeButton.addActionListener(handler);
    container.add(threeButton);
    fourButton = new JButton("4");
    fourButton.addActionListener(handler);
    container.add(fourButton);
    fiveButton = new JButton("5");
    fiveButton.addActionListener(handler);
    container.add(fiveButton);
    sixButton = new JButton("6");
    sixButton.addActionListener(handler);
    container.add(sixButton);
    sevenButton = new JButton("7");
    sevenButton.addActionListener(handler);
    container.add(sevenButton);
    eightButton = new JButton("8");
    eightButton.addActionListener(handler);
    container.add(eightButton);
    nineButton = new JButton("9");
    nineButton.addActionListener(handler);
    container.add(nineButton);
    zeroButton = new JButton("0");
    zeroButton.addActionListener(handler);
    container.add(zeroButton);
    openBracketButton = new JButton("(");
    openBracketButton.addActionListener(handler);
    container.add(openBracketButton);
    closeBracketButton = new JButton(")");
    closeBracketButton.addActionListener(handler);
    container.add(closeBracketButton);
    decimalPointButton = new JButton(".");
    decimalPointButton.addActionListener(handler);
    container.add(decimalPointButton);
    //Operator buttons.
    additionButton = new JButton("+");
    additionButton.addActionListener(handler);
    container.add(additionButton);
    subtractionButton = new JButton("-");
    subtractionButton.addActionListener(handler);
    container.add(subtractionButton);
    multiplicationButton = new JButton("*");
    multiplicationButton.addActionListener(handler);
    container.add(multiplicationButton);
    divisionButton = new JButton("/");
    divisionButton.addActionListener(handler);
    container.add(divisionButton);
    //Field Modifier buttons.
    answerButton = new JButton("Answer");
    answerButton.addActionListener(handler);
    container.add(answerButton);
    clearButton = new JButton("Clear");
    clearButton.addActionListener(handler);
    container.add(clearButton);
    clearAllButton = new JButton("Clear All");
    clearAllButton.addActionListener(handler);
    container.add(clearAllButton);
    //
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.pack();
    this.setVisible(true);
    //
    calc.initializeOperators();
  }
  
  /*
   * Implements the ActionListener class that responds to the various
   * button presses and text field actions.
   */
  private class InputHandler implements ActionListener
  {
    /*
     * Called when an action occurs e.g. a button is pressed or enter
     * is pressed while the cursor is in a text field.
     * This method takes appropriate action to respond to user
     * interaction with the user interface.
     */
    public void actionPerformed(ActionEvent event)
    {
      String currentInputText = inputTextField.getText();
      if (event.getSource() == inputTextField || event.getSource() == answerButton)
      {
        String result = calc.getResult(inputTextField.getText());
        boolean isNumber = Functions.stringIsDouble(result);
        if (isNumber)
        {
          outputTextField.setText(currentInputText + "=" + result);
          inputTextField.setText(result);
        }
        else
        {
          outputTextField.setText(result);
        }
      }
      else if (event.getSource() == zeroButton)
      {
        inputTextField.setText(currentInputText + "0");
      }
      else if (event.getSource() == oneButton)
      {
        inputTextField.setText(currentInputText + "1");
      }
      else if (event.getSource() == twoButton)
      {
        inputTextField.setText(currentInputText + "2");
      }
      else if (event.getSource() == threeButton)
      {
        inputTextField.setText(currentInputText + "3");
      }
      else if (event.getSource() == fourButton)
      {
        inputTextField.setText(currentInputText + "4");
      }
      else if (event.getSource() == fiveButton)
      {
        inputTextField.setText(currentInputText + "5");
      }
      else if (event.getSource() == sixButton)
      {
        inputTextField.setText(currentInputText + "6");
      }
      else if (event.getSource() == sevenButton)
      {
        inputTextField.setText(currentInputText + "7");
      }
      else if (event.getSource() == eightButton)
      {
        inputTextField.setText(currentInputText + "8");
      }
      else if (event.getSource() == nineButton)
      {
        inputTextField.setText(currentInputText + "9");
      }
      else if (event.getSource() == openBracketButton)
      {
        inputTextField.setText(currentInputText + "(");
      }
      else if (event.getSource() == closeBracketButton)
      {
        inputTextField.setText(currentInputText + ")");
      }
      else if (event.getSource() == decimalPointButton)
      {
        inputTextField.setText(currentInputText + ".");
      }
      else if (event.getSource() == clearButton)
      {
        if (currentInputText.length() > 0)
        {
          int minusOneCharIndex = currentInputText.length() - 1;
          String textMinusOneChar = currentInputText.substring(0, minusOneCharIndex);
          inputTextField.setText(textMinusOneChar);
        }
      }
      else if (event.getSource() == clearAllButton)
      {
        inputTextField.setText("");
      }
      else if (event.getSource() == additionButton)
      {
        inputTextField.setText(currentInputText + "+");
      }
      else if (event.getSource() == subtractionButton)
      {
        inputTextField.setText(currentInputText + "-");
      }
      else if (event.getSource() == multiplicationButton)
      {
        inputTextField.setText(currentInputText + "*");
      }
      else if (event.getSource() == divisionButton)
      {
        inputTextField.setText(currentInputText + "/");
      }
    }
  }
}
