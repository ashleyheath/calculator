import java.util.ArrayList;

/**
 * The Addition class implements addition for the calculator.
 */
public class Addition extends Operation
{

  public String getOperatorSymbol()
  {
    return new String("+");
  }

  public double getPriority()
  {
    return 1;
  }

  public int getNumberOfOperandsRequired()
  {
    return 2;
  }

  public ArrayList operate(int operatorIndex, int operandIndex, ArrayList<Operation> operatorList, ArrayList<Operand> operandList)
  {    
    boolean somethingToAdd = operandList.size() > 1;
    if (somethingToAdd)
    {
      Operand indexOperand = operandList.get(operandIndex);
      double indexOperandValue = indexOperand.getOperandValue();
      Operand indexPlusOneOperand = operandList.get(operandIndex+1);
      double indexPlusOneOperandValue = indexPlusOneOperand.getOperandValue();
      
      double operationResult = indexOperandValue + indexPlusOneOperandValue;
      Operand newOperand = new Operand(operationResult, this.getPriorityIncrease());
      operandList.set(operandIndex, newOperand);
      operandList.remove(operandIndex+1);
    }

    operatorList.remove(operatorIndex);
    
    ArrayList<ArrayList> operatorAndOperandLists = new ArrayList<ArrayList>();
    operatorAndOperandLists.add(0, operatorList);
    operatorAndOperandLists.add(1, operandList);
    return operatorAndOperandLists;
  }
}
