/**
 * The Operand class provides an Operand object which stores the 
 * data needed by an operand and its priority increase.
 */
public class Operand
{
  private double operandValue_;
  private double priorityIncrease_;

  /*
   * Creates the operand object.
   * 
   * @param operandValue The value of the operand.
   * @param priorityIncrease The priority increase of the operand.
   */
  public Operand(double operandValue, double priorityIncrease)
  {
    this.operandValue_ = operandValue;
    this.priorityIncrease_ = priorityIncrease;
  }
  
  /*
   * Returns the value of the operand.
   * the new string.
   * 
   * @returns this.operandValue_ The value of the operand.
   */
  public double getOperandValue()
  {
    return this.operandValue_;
  }
  
  /*
   * Returns the priority increase of the operand.
   * 
   * @returns this.priorityIncrease_ the priority increase of the
   *          operand.
   */
  public double getPriorityIncrease()
  {
    return this.priorityIncrease_;
  }
}
