import java.io.Console;

/**
 * The class Calculator is the main class of the calculator
 * program.
 */
public class Calculator
{

  /*
   * The main function of the class. Based on the command line
   * arguments provided the calculator program is run in either
   * command line or graphical interface mode.
   * If no arguments are provided the program defaults to the
   * interactive console mode (i.e. --text).
   * The possible arguments are:
   * --gui Runs the calculator with a graphical interface.
   * --text Runs the calculator interactively in the console.
   * Any other string given as an argument will be fed straight to
   * the calculator and its output printed to the console before
   * ending the program.
   */
  public static void main(String[] args)
  {
    String guiArg = "--gui";
    String textArg = "--text";
    CalculatorObject calc = new CalculatorObject();
    calc.initializeOperators();
    Console calcConsole = System.console();
    //Merge arguments into one string
    String argsString = "";
    for (int i = 0; i<args.length; i++)
    {
      argsString += args[i];
    }
    argsString = Functions.stripSpacesAndTabs(argsString);
    boolean noInput = argsString.equals("");
    boolean modeIsGui = argsString.equals(guiArg);
    boolean modeIsText = argsString.equals(textArg);
    if (noInput)
    {
      modeIsText = true;
    }
    if (modeIsGui)
    {
      Gui calcGui = new Gui();
    }
    else if (modeIsText)
    {
      while (true)
      {
        String inputString = calcConsole.readLine("\n>>>");
        String result = calc.getResult(inputString);
        calcConsole.printf(result);
      }
    }
    else
    {
      String result = calc.getResult(argsString);
      calcConsole.printf(result);
    }
  }
}
