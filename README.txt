Run the program by running 'java Calculator' from the command line.
Use the argument '--gui' to launch the graphical user interface.
Any other argument is interpreted as a calculation.
Providing no argument launches the interactive command line interface. This may be quit at any time by pressing 'CTRL-c'.

Assignment is done by typing any character that is not used by the calculator followed by the '=' symbol and then the calculation you wish to store.
You cannot assign something to the same character more than once.
