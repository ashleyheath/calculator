import java.util.ArrayList;

/**
* The Subtraction implements subtraction and negation for the calculator.
*/
public class Subtraction extends Operation
{

  public String getOperatorSymbol()
  {
    return "-";
  }

  public double getPriority()
  {
    return 3;
  }

  public int getNumberOfOperandsRequired()
  {
    return 1;
  }

  public ArrayList operate(int operatorIndex, int operandIndex, ArrayList<Operation> operatorList, ArrayList<Operand> operandList)
  {
    int nullOperandsRemoved = 0;
    Operand operandAtIndex = operandList.get(operandIndex);
    boolean replaceWithAddition = false;
    if (operandAtIndex instanceof NullOperand == false)
    {
      operandIndex++;
      replaceWithAddition = true;
    }
    boolean looping = true;
    while (looping)
    {
      operandAtIndex = operandList.get(operandIndex);
      if (operandAtIndex instanceof NullOperand)
      {
        operandList.remove(operandIndex);
        nullOperandsRemoved++;
        operatorList.remove(operatorIndex);
      }
      else
      {
        if (nullOperandsRemoved % 2 != 0)
        {
          double result = -1 * operandAtIndex.getOperandValue();
          Operand resultOperand = new Operand(result, this.getPriorityIncrease());
          operandList.set(operandIndex, resultOperand);
        }
        if (replaceWithAddition)
        {
          if (operatorList.size() == 0)
          {
            Addition add = new Addition();
            add.setPriorityIncrease(this.getPriorityIncrease());
            operatorList.add(add);
          }
          else
          {
            Addition add = new Addition();
            add.setPriorityIncrease(this.getPriorityIncrease());
            operatorList.add(operatorIndex, add);
          }
        }
        looping = false;
      }
    }
    ArrayList<ArrayList> operatorAndOperandLists = new ArrayList<ArrayList>();
    operatorAndOperandLists.add(0, operatorList);
    operatorAndOperandLists.add(1, operandList);
    return operatorAndOperandLists;
  }
}
