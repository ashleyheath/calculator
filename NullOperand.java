/**
 * The NullOperand class provides extends the Operand class to
 * provide unary operations with a dummy operand to make them behave
 * like binary operations.
 */
public class NullOperand extends Operand
{

  /*
   * Creates the NullOperand object using giving the operand a
   * priority of -1 which is impossible to achieve under normal
   * use of the calculator.
   */
  public NullOperand()
  {
    super(0, -1);
  }
}
