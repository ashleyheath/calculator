/**
 * The OperationException class provides a custom exception for the
 * calculator to throw when an operator's safeToOperate test is
 * failed.
 */
public class OperationException extends Exception
{
  private String errorMessage_;

  /*
   * Creates the exception and stores the provided error message.
   *
   * @param errorMessageArgument The error message to be stored.
   */
  public OperationException(String errorMessageArgument)
  {
    this.errorMessage_ = errorMessageArgument;
  }
  
  /*
   * Returns the exception's error message.
   *
   * @returns this.errorMessage_ The stored error message.
   */
  public String getErrorMessage()
  {
    return this.errorMessage_;
  }
}
