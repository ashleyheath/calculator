/**
 * The Functions class provides several reusable standard functions
 * that are used by various parts of the calculator.
 */
public class Functions
{
  /*
   * Removes the spaces and tabs from the given string and returns
   * the new string.
   * 
   * @param inputString The string to be stripped of whitespaces and
   *        tabs.
   * @returns modifiedString The original string stripped of
   *        whitespaces and tabs.
   */
  public static String stripSpacesAndTabs(String inputString)
  {
    String tabCharacter = "\t";
    String modifiedString = "";
    for (int i = 0; i<inputString.length(); i++)
    {
      char currentChar = inputString.charAt(i);
      String currentCharString = Character.toString(inputString.charAt(i));
      boolean isWhitespace = Character.isWhitespace(currentChar);
      boolean isTab = currentCharString.equals(tabCharacter);
      if (isWhitespace==false && isTab==false)
      {
        modifiedString += Character.toString(currentChar);
      }      
    }
    return modifiedString;
  }

  /*
   * Checks to see if the String a is an element of the 
   * the array b.
   * 
   * @param stringA The string to be looked for in the array.
   * @param arrayB The array of strings to search in.
   * @returns true If a match is found, else it returns false.
   */
  public static boolean aInB(String stringA, String[] arrayB)
  {
    for(int i = 0; i < arrayB.length; i++)
    {
      boolean stringsAreEqual = stringA.equals(arrayB[i]);
      if (stringsAreEqual)
      {
        return true;
      }
    }
    return false;
  }

  /*
   * Checks to see if the String testString is an integer.
   * 
   * @param testString The string to be checked against the numbers.
   * @returns true If a match is found, else it returns false.
   */
  public static boolean stringIsInt(String testString)
  {
    String[] numbers = {"0","1","2","3","4","5","6","7","8","9"};
    int testStringLength = testString.length();
    for (int i = 0; i<testStringLength; i++)
    {
      for (int j = 0; j<numbers.length; j++)
      {
        char testCharacter = testString.charAt(i);
        String toTest = Character.toString(testCharacter);
        boolean stringsMatch = toTest.equals(numbers[j]);
        if (stringsMatch)
        {
          return true;
        }
      }
    }
    return false;
  }

  /*
   * Checks to see if the String testString is a double.
   * 
   * @param testString The string to be checked against the numbers.
   * @returns true If a match is found, else it returns false.
   */
  public static boolean stringIsDouble(String testString)
  {
    String[] allowed = {"0","1","2","3","4","5","6","7","8","9",".","e"};
    int testStringLength = testString.length();
    for (int i = 0; i<testStringLength; i++)
    {
      String toTest = Character.toString(testString.charAt(i));
      boolean matchFound = aInB(toTest, allowed);
      if (!matchFound)
      {
        return false;
      }
    }
    return true;
  }
}
