import java.util.ArrayList;

/**
 * The Operation class provides an abstract implementation on which
 * new operations may be based.
 */
public abstract class Operation implements Cloneable
{ 
  /*
   * priorityIncrease
   * This is the amount above its standard priority level a
   * particular instance of an operation is set to.
   */
  private double priorityIncrease_ = 0;

  /*
   * Changes the operation's priority increase to the value given.
   * 
   * @param increase The value to set the priority increase to.
   */
  public void setPriorityIncrease(double increase)
  {
    this.priorityIncrease_ = increase;
  }
  
  /*
   * Creates a public way of accessing the clone method.
   */
  public Operation clone()
  {
    try
    {
      return (Operation) super.clone();
    }
    catch(Exception e)
    {
      return null;
    }
  }

  /*
   * Returns the priority increase of a particular instance of 
   * this operation.
   * 
   * @returns this.priorityIncrease_ The base priortiy increase of
   *          this operation.
   */
  public double getPriorityIncrease()
  {
    return this.priorityIncrease_;
  }

  /*
   * Returns the operation's base priority value in addition to the
   * particular instance's priority increase.
   * 
   * @returns this.getPriority() + this.priorityIncrease_ The base
   *          priority plus the priority increase.
   */
  public double getPriorityPlusIncrease()
  {
    return this.getPriority() + this.priorityIncrease_;
  }

  /*
   * Designed to be overridden by operations that need to test if
   *  the provided operand(s) are safe to use.
   * 
   * @param operatorIndex Where the calculator is in the operator
   *        list. 
   * @param operandIndex Where the calculator is in the operand list.
   * @param operatorList List of operators in the input string.
   * @param operandList List of operands in the input string.
   * @returns true By default as most operators do not need to test
   *        for safe inputs.
   */
  public boolean safeToOperate(int operatorIndex, int operandIndex, ArrayList<Operation> operatorList, ArrayList<Operand> operandList)
  {
    return true;
  }

  /*
   * Returns the error message to display if the safeToOperate test
   * fails. Designed to be overridden by operations that make use of
   * the safeToOperate method.
   * 
   * @returns new String("") An empty string, which is the default
   *          error message.
   */
  public String getErrorMessage()
  {
    return new String("");
  }
  
  /*
   * To be called by the operate method to adjust the index if the
   * operation requires more than one operand. Returns the adjusted
   * operand index. The index of the operator should be stored 
   * seperately before this is called.
   *
   * @param operandIndex Where the calculator is currently in the 
   *        operand list.
   * @param operandList List of operands in the input string.
   * @returns adjustedIndex The operand index, adjusted for 
   *        NullOperands if the operation requires more than one
   *        operand.
   */
  public int getOperandIndex(int operatorIndex, int operandIndex, ArrayList<Operation> operatorList, ArrayList<Operand> operandList)
  {
    int adjustedIndex = operandIndex;
    boolean moreThanOneOperand = this.getNumberOfOperandsRequired() > 1;
    if (moreThanOneOperand)
    {
      for (int i = 0; i<=operandIndex; i++)
      {
        if (operandList.get(i) instanceof NullOperand)
        {
          adjustedIndex++;
        }
      }
      //Avoid going out of bounds
      if (adjustedIndex >= operandList.size()-1-1)
      {
        adjustedIndex = operandList.size()-1-1;
      }
      if (adjustedIndex < 0)
      {
        adjustedIndex = 0;
      }
    }
    else
    {
      //If the previous operand is null don't adjust
      if (operatorIndex-1 >= 0
         &&operandList.get(operatorIndex-1) instanceof NullOperand)
      {
        adjustedIndex = operatorIndex;
      }
      else
      {
        for (int i = 0; i<operatorIndex; i++)
        {
          Operation op = operatorList.get(i);
          if (op.getNumberOfOperandsRequired() < 2)
          {    
            adjustedIndex +=1;
          }
        }
      }
    }
    return adjustedIndex;
  }

  /*
   * Return the base priority of the operator.
   */
  abstract double getPriority();

  /*
   * Return the operator symbol as a string.
   */
  abstract String getOperatorSymbol();

  /*
   * Return the number of operands the operation requires
   * i.e. is it unary, binary etc.
   */
  abstract int getNumberOfOperandsRequired();
  
  /*
   * Returns an ArrayList containing the operatorList and operandList
   * after the operation has been performed.
   * 
   * @param operatorIndex Where the calculator is in the operator
   *        list. 
   * @param operandIndex Where the calculator is in the operand list.
   * @param operatorList List of operators in the input string.
   * @param operandList List of operands in the input string.
   */
  abstract ArrayList operate(int operatorIndex, int operandIndex, ArrayList<Operation> operatorList, ArrayList<Operand> operandList);
}
