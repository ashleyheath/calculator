import java.util.ArrayList;

/**
 * The Division class implements division for the calculator.
 */
public class Division extends Operation
{

  public String getOperatorSymbol()
  {
    return new String("/");
  }

  public double getPriority()
  {
    return 2;
  }

  public String getErrorMessage()
  {
    return new String("Divide by 0.");
  }

  public int getNumberOfOperandsRequired()
  {
    return 2;
  }

  public boolean safeToOperate(int operatorIndex, int operandIndex, ArrayList<Operation> operatorList, ArrayList<Operand> operandList)
  {
    int yIndex = operandIndex+1;
    Operand yOperand = operandList.get(yIndex);
    double y = yOperand.getOperandValue();
    if(y == 0)
    {
      return false;
    }
    else
    {
      return true;
    }
  }

  public ArrayList operate(int operatorIndex, int operandIndex, ArrayList<Operation> operatorList, ArrayList<Operand> operandList)
  {  
    Operand indexOperand = operandList.get(operandIndex);
    double indexOperandValue = indexOperand.getOperandValue();
    Operand indexPlusOneOperand = operandList.get(operandIndex+1);
    double indexPlusOneOperandValue = indexPlusOneOperand.getOperandValue();
      
    double operationResult = indexOperandValue / indexPlusOneOperandValue;
    Operand newOperand = new Operand(operationResult, this.getPriorityIncrease());
    operandList.set(operandIndex, newOperand);
    operandList.remove(operandIndex+1);

    operatorList.remove(operatorIndex);
    
    ArrayList<ArrayList> operatorAndOperandLists = new ArrayList<ArrayList>();
    operatorAndOperandLists.add(0, operatorList);
    operatorAndOperandLists.add(1, operandList);
    return operatorAndOperandLists;
  }
}
